FROM archlinux:20200407
MAINTAINER Marc 'risson' Schmitt <marc.schmitt@risson.space>

RUN pacman --noconfirm --noprogressbar -Syyu

RUN pacman --noconfirm --noprogressbar --needed -S make gcc \
            clang cmake gcc-libs glibc boost gdb pacman-contrib \
            valgrind python python-yaml python-termcolor gtest

RUN paccache -r
